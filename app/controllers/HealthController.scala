package controllers

import javax.inject.Singleton

import play.api.mvc.{Action, Controller}

/**
  * Health controller
  */
@Singleton
class HealthController extends Controller {

  /**
    * Health endpoint
    */
  def index = Action { Ok }

}
