package controllers

import javax.inject.{Inject, Singleton}

import data.{Statistics, Transaction}
import play.api.libs.json.{JsError, Json, Writes}
import play.api.mvc.{Action, Controller}
import services.{CacheService, StatisticsService}
import scala.util.{Failure, Success}

/**
  * Statistics Controller
  * @param cacheService
  * @param statisticsService
  */
@Singleton
class StatisticsController @Inject()(cacheService: CacheService, statisticsService: StatisticsService) extends Controller{

  implicit val statisticsWriter = Json.writes[Statistics]
  implicit val transactionReader = Json.reads[Transaction]

  /**
    * Get cache statistics
    * @return HttpResponse
    */
  def index = Action { implicit request =>
    Ok(Json.toJson(statisticsService.getStatistics))
  }

  /**
    * Add a transaction to the cache
    * @return HttpResponse
    */
  def add = Action(parse.json) { implicit request =>
    request.body.validate[Transaction].map{
      case (transaction) => {
        cacheService.put(transaction) match {
          case Success(_) => Created
          case Failure(_) => NoContent
        }
      }
    }.recoverTotal{
      e => BadRequest("Detected error: "+ JsError.toJson(e))
    }
  }
}
