package services

import java.util.concurrent.TimeUnit
import javax.inject.{Inject, Singleton}

import com.github.benmanes.caffeine.cache.{Caffeine, Cache => CaffeineCache}
import data.Transaction

import collection.JavaConverters._
import scala.util.{Failure, Success, Try}
import java.lang.{Long => BoxedLong}

import org.joda.time.DateTime

import scala.concurrent.duration._

/**
  * Time based transaction cache service
  */
@Singleton
class CacheService @Inject()(transactionValidationService: TransactionValidationService) {

  // we don't care about the map keys so just use an incremented index
  // that will wrap around at Long.MAX_VALUE. this means multiple values
  // for same timestamp can be stored
  private var index = 0L

  private val cache: CaffeineCache[BoxedLong, Transaction] = Caffeine
    .newBuilder()
    .expireAfterWrite(60, TimeUnit.SECONDS)
    .recordStats()
    .build[BoxedLong, Transaction]()

  /**
    * Store a transaction in the cache if it is valid
    * @param transaction
    * @return Try[Unit]
    */
  def put(transaction: Transaction): Try[Unit] = {
    if (transactionValidationService.isValid(transaction)) {
      cache.put(index, transaction)
      index += 1
      Success(())
    } else {
      Failure(new Exception(s"Transaction is not valid"))
    }
  }

  def stats = {
    cache.stats()
  }

  def values = {
    cache.asMap().values().asScala.toSeq
  }
}
