package services

import javax.inject.{Inject, Singleton}

import data.Statistics

/**
  * Statistics calculation service
  * @param cacheService
  */
@Singleton
class StatisticsService @Inject()(cacheService: CacheService) {

  /**
    * Get a statistical overview of CacheService
    * @return Statistics
    */
  def getStatistics: Statistics = {
    val transactions = cacheService.values.sortBy(_.amount)
    transactions.length match {
      case 0 => Statistics(0, 0, 0, 0, 0)
      case _ => {
        val sum = transactions.map(_.amount).sum
        Statistics(
          sum = sum,
          avg = sum / transactions.length,
          max = transactions.last.amount,
          min = transactions.head.amount,
          count = transactions.length.toLong
        )
      }
    }
  }

}
