package services

import data.Transaction
import org.joda.time.DateTime
import scala.concurrent.duration._

class TransactionValidationService {

  private val MAX_AGE = 60.seconds

  /**
    * Is transaction less than MAX_AGE old?
    * @param transaction
    * @param now
    * @return Boolean
    */
  def isValid(transaction: Transaction, now: DateTime = DateTime.now): Boolean = {
    now.getMillis() / 1000 - transaction.timestamp <= MAX_AGE.toSeconds
  }
}
