import play.api.http.HttpErrorHandler
import play.api.mvc._
import play.api.mvc.Results._

import scala.concurrent._
import javax.inject.Singleton

import play.api.libs.json.Json;

case class ErrorResponse(message: String)

/**
  * Custom error handler with JSON responses
  */
@Singleton
class ErrorHandler extends HttpErrorHandler {

  implicit val writesErrorResponse = Json.writes[ErrorResponse]

  def onClientError(request: RequestHeader, statusCode: Int, message: String) = {
    Future.successful(
      Status(statusCode)(Json.toJson(ErrorResponse("A client error occurred: " + message)))
    )
  }

  def onServerError(request: RequestHeader, exception: Throwable) = {
    Future.successful(
      InternalServerError(Json.toJson(ErrorResponse("A server error occurred: " + exception.getMessage)))
    )
  }
}