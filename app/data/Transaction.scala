package data

case class Transaction(amount: Double, timestamp: Long)
