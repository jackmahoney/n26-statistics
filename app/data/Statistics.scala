package data

case class Statistics(sum: Double, avg: Double, max: Double, min: Double, count: Long)
