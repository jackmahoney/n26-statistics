.PHONY: test run

test:
	sbt test

run:
	sbt run

start:
	sbt start