# n26 backend challenge

## About
Built in 3 hours using Scala Play Framework and Caffeine cache. It is a simple API but I concentrated on separation of concerns, clean code,
documentation, dependency injection, and testing. I did not write my own time based cache, I used a library instead. This most accurately
reflects a real life scenario. 

## Run
`make start` starts server in production mode on `localhost:9000`.

## Dev
`make run` starts dev server with hot-swap on `localhost:9000`

## Test
`make test` runs specs2 unit tests.

## API
`curl -i http://localhost:9000/health`

`curl -i http://localhost:9000/statistics`

`curl -i -X POST -H "Content-Type: application/json" -d '{"amount":<amount:Double   >, "timestamp":<timestamp:Long>}' http://localhost:9000/transactions`