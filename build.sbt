import sbt.Keys._

scalaVersion := "2.11.11"
libraryDependencies += "com.github.ben-manes.caffeine" % "caffeine" % "2.5.2"
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "2.0.0" % Test
libraryDependencies += ws
libraryDependencies += specs2 % Test

lazy val playRestApi = (project in file(".")).enablePlugins(Common, PlayScala)
.settings(
  name := """n26-statistics"""
)
