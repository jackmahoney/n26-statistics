import data.{Statistics, Transaction}
import org.junit.runner._
import org.specs2.mock.Mockito
import org.specs2.mutable._
import org.specs2.runner._
import services.{CacheService, StatisticsService}

@RunWith(classOf[JUnitRunner])
class StatisticsServiceSpec extends Specification with Mockito{

  "#getStatistics" should {

    "returns zeroed statistics when no transactions" in {
      val cacheService = mock[CacheService]
      cacheService.values returns Seq.empty
      val statisticsService = new StatisticsService(cacheService)
      statisticsService.getStatistics mustEqual Statistics(0,0,0,0,0)
    }

    "return transaction values when 1 transactions" in {
      val cacheService = mock[CacheService]
      cacheService.values returns Seq(Transaction(12,0))
      val statisticsService = new StatisticsService(cacheService)
      statisticsService.getStatistics mustEqual Statistics(12,12,12,12,1)
    }

    "return calculated values when 2 transactions" in {
      val cacheService = mock[CacheService]
      cacheService.values returns Seq(Transaction(12,0), Transaction(5,0))
      val statisticsService = new StatisticsService(cacheService)
      statisticsService.getStatistics mustEqual Statistics(12 + 5,(12 + 5)/2d,12,5,2)
    }
  }

}
