import data.Transaction
import org.joda.time.DateTime
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import services.{CacheService, TransactionValidationService}

import scala.concurrent.duration._

@RunWith(classOf[JUnitRunner])
class TransactionValidationServiceSpec extends Specification {

  val cacheService = new TransactionValidationService
  val now = DateTime.now

  "#isValid" should {
    "return false for entries older than 60 seconds" in {
      val transaction = Transaction(0, now.getMillis / 1000)
      cacheService.isValid(transaction, now.plus(61.seconds.toMillis)) mustEqual false
    }
    "return true for entries 60 seconds old" in {
      val transaction = Transaction(0, now.getMillis / 1000)
      cacheService.isValid(transaction, now.plus(60.seconds.toMillis)) mustEqual true
    }
    "return false for entries less than 60 seconds old" in {
      val transaction = Transaction(0, now.getMillis / 1000)
      cacheService.isValid(transaction, now.plus(1.seconds.toMillis)) mustEqual true
    }
  }

}
