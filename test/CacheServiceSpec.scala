import data.Transaction
import org.joda.time.DateTime
import org.junit.runner._
import org.specs2.mock.Mockito
import org.specs2.mutable._
import org.specs2.runner._
import services.{CacheService, TransactionValidationService}

import scala.concurrent.duration._
import scala.util.Failure

@RunWith(classOf[JUnitRunner])
class CacheServiceSpec extends Specification with Mockito{

  "#put" should {
    "return Failure if transaction is invalid" in {
      val transaction = Transaction(0, 0)
      val transactionValidationService = mock[TransactionValidationService]
      transactionValidationService.isValid(transaction) returns false
      val cacheService = new CacheService(transactionValidationService)

      cacheService.put(transaction) must beFailedTry
      cacheService.values.length mustEqual 0
    }
    "return Success if transaction is valid" in {
      val transaction = Transaction(0, 0)
      val transactionValidationService = mock[TransactionValidationService]
      transactionValidationService.isValid(transaction) returns true
      val cacheService = new CacheService(transactionValidationService)

      cacheService.put(transaction) must beSuccessfulTry
      cacheService.values.length mustEqual 1
    }
    "allow duplicate values" in {
      val transaction = Transaction(0, 0)
      val transactionValidationService = mock[TransactionValidationService]
      transactionValidationService.isValid(transaction) returns true
      val cacheService = new CacheService(transactionValidationService)

      cacheService.put(transaction) must beSuccessfulTry
      cacheService.put(transaction) must beSuccessfulTry
      cacheService.values.length mustEqual 2
    }
  }

}
