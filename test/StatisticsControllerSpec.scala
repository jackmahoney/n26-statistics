import akka.stream.Materializer
import controllers.StatisticsController
import data.{Statistics, Transaction}
import org.junit.runner._
import org.specs2.mutable._
import org.scalatestplus.play._
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import org.specs2.mock.Mockito
import org.specs2.runner._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test.{FakeRequest, WithApplication}
import play.mvc.Results
import services.{CacheService, StatisticsService}

import scala.concurrent.Future
import scala.util.{Failure, Success}

@RunWith(classOf[JUnitRunner])
class StatisticsControllerSpec extends PlaySpec with Mockito with GuiceOneAppPerSuite {

  implicit lazy val materializer: Materializer = app.materializer
  implicit val transactionWriter = Json.writes[Transaction]

  "#index" should {

    "return 200 and statistics json" in {
      val cacheService = mock[CacheService]
      val statisticsService = mock[StatisticsService]
      val statistics = Statistics(0,0,0,0,0)
      statisticsService.getStatistics returns statistics

      val controller = new StatisticsController(cacheService, statisticsService)
      val request = FakeRequest(GET, "/statistics")
      val result = call(controller.index, request)

      status(result) mustEqual OK
      contentAsJson(result) mustEqual Json.toJson(statistics)(controller.statisticsWriter)
    }

  }

  "#add" should {

    "return 400 for invalid json" in {
      val cacheService = mock[CacheService]
      val statisticsService = mock[StatisticsService]
      val transaction = Transaction(0,0)
      cacheService.put(transaction) returns Failure(new Exception(""))
      val controller = new StatisticsController(cacheService, statisticsService)
      val request = FakeRequest(POST, "/transaction")
      val result = call(controller.add, request.withJsonBody(Json.parse("""{"foo":"bar"}""")))

      status(result) mustEqual BAD_REQUEST
    }

    "return 201 for valid transaction" in {
      val cacheService = mock[CacheService]
      val statisticsService = mock[StatisticsService]
      val transaction = Transaction(0,0)
      cacheService.put(transaction) returns Success(())
      val controller = new StatisticsController(cacheService, statisticsService)
      val request = FakeRequest(POST, "/transaction")
      val result = call(controller.add, request.withJsonBody(Json.toJson(transaction)))

      status(result) mustEqual CREATED
    }
    "return 204 for invalid transaction" in {
      val cacheService = mock[CacheService]
      val statisticsService = mock[StatisticsService]
      val transaction = Transaction(0,0)
      cacheService.put(transaction) returns Failure(new Exception(""))
      val controller = new StatisticsController(cacheService, statisticsService)
      val request = FakeRequest(POST, "/transaction")
      val result = call(controller.add, request.withJsonBody(Json.toJson(transaction)))

      status(result) mustEqual NO_CONTENT
    }

  }

}
